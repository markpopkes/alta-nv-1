</main>

<footer class="footer">
  <div class="grid-container full">
    <div class="grid-x align-center align-middle text-center">
        <div class="cell small-12 medium-12 large-12">
            <img class="footer__logo" src="/wp-content/themes/alta-nv/assets/images/logo-footer.png" alt="alta-nv-logo" />
            <div class="footer__info">
                <a href="tel:833.467.7360">833.467.7360</a>
                <span>1250 Wigwam Pkwy, Henderson, NV 89074</span>
            </div> <!-- .footer__links -->
            <div class="footer__credits">
                <span>&copy; 2019 Alta NV | Professionally Managed by Wood Residential Services | <a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a></span>
                <img src="/wp-content/themes/alta-nv/assets/images/logos-footer.png" alt="alta-nv-logo" />
            </div> <!-- .footer__credits -->
        </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</footer>

</div> <!-- .cell -->
</div> <!-- .grid-x -->
</div> <!-- .grid-container -->
</div> <!-- .#wrapper -->

<?php wp_footer(); ?>

<script src="https://livealtanv.fatwin.com/api/websites/resources/1?x=ZnJXdVJXK2V6Mi9ZV2JnUnRSNyt1aHZyZnVPdVZRY3ZHcHFrbzdYRlhNOTdOTVVqUmlmSVZRbHJCUGtuaUxEbFB5SHdhWlJBZ0llcUc5UUtRYzNEbUE9PQ2"></script>

</body>
</html>
