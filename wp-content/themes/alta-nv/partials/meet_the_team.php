<?php

  $team_member = get_sub_field('meet_the_team'); ?>

  
  <section id="section<?= get_row_index(); ?>" class="grid-container meet_the_team">
    
      <?php foreach ($team_member['team'] as $key => $member):
        $image1 = $member['image1']; 
        $title = $member['title'];
        $name = $member['name'];
        $summary = $member['summary']; ?>
        <div class="team-member grid-x align-middle<?= ($key % 2 == 1 ? ' even' : ''); ?>">
          <div class="section-images cell large-6">
            <div class="image-container">
              <div class="img-1">
                <div><img src="<?= (!empty($image1["url"]) ? $image1["url"] : ""); ?>" alt="Alta NV"/></div>
              </div>
            </div> <!-- .image-container -->
          </div> <!-- .cell -->

          <div class="section-content cell large-6">
            <h2><?= $name; ?></h2>
            <h4><?= $title; ?></h4>
            <div class="copy">
              <?= $summary; ?>
            </div> <!-- .copy -->
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      <?php endforeach; ?>
  </section> <!-- .section -->