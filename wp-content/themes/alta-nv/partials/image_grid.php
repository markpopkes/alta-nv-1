<?php

  $expanded_grid = get_sub_field('expanded_grid'); 
  $bg_color = get_sub_field('background_color'); 
  $count = 0;
  $images = get_sub_field('grid_images');

  $images_lightbox = get_sub_field('lightbox_images');
  $numberOfColumns = 4;
  $bootstrapColWidth = 12 / $numberOfColumns;

  $arrayChunks = array_chunk($images, $numberOfColumns) ?>

  <section id="section<?= get_row_index(); ?>" class="section gallery-slider" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
    <div class="grid-container full">
      <div class="grid-x align-center">
        <div class="cell large-12">
          <div class="image-slider-section relative">
            <?php if(!empty($images_lightbox)): ?>
              <div class="slide-wrapper">
                <div id="image-altanv-slider" class="image-slider" data-index="<?= get_row_index(); ?>">
                  <?php foreach($images_lightbox as $image): ?>
                    <?php 
                        $slide_img = wp_get_attachment_image_src( $image['ID'], 'full'); ?>
                        <div class="image-slide" style="background-image: url(<?= $slide_img[0]; ?>); background-position: center center"></div>
                    <?php $count++; endforeach; ?>
                </div> <!-- #image-slider --> 
                <div class="slide-controls slide-controls">
                  <div class="slide-counter"></div>
                </div> <!-- .slide-controls -->
              </div> <!-- .slide-wrapper -->
            <?php endif; ?>
          </div> <!-- .image-slider-section -->

          <div class="image-grid-section">
            <div class="image-grid photos" data-slider="altanv-slider">
              <div class="grid-container <?= ($expanded_grid == true && $switch_columns == false ? 'grid-expanded' : ''); ?>">
                <?php
                  if($images):
                    foreach($images as $lightbox):
                      $image_array[] = $lightbox['url'];
                    endforeach;
                  endif;

                  $count = 0;

                  foreach($arrayChunks as $items):
                    echo '<div class="grid-x align-center text-center gallery-container">';
                    foreach($items as $item):
                      echo '<div class="cell small-12 medium-12 gallery-item large-'.$bootstrapColWidth.'">';
                      //echo '<a href="'.$image_array[$count].'" data-effect="mfp-zoom-in">';
                      echo '<a href="#" data-slide="'.$count.'"><img src="'.$item['url'].'" alt="grid-image" /></a>';
                      //echo '</a>';
                      echo '</div>';
                    $count++; endforeach;
                    echo '</div>';
                  endforeach;
                  ?>
              </div> <!-- .grid-container -->
            </div> <!-- .image-grid -->
          </div> <!-- .image-grid-section -->
        </div> <!-- .cell -->
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->
</section> <!-- section -->