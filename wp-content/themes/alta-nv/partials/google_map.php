<?php
  $count = 1;
  $locations = get_sub_field('locations');
  $copy = get_sub_field('copy');
  $bg_color = get_sub_field('bg_color');
  $map = get_sub_field('google_map');
  $switch_columns = get_sub_field('switch_columns');
  $expanded_grid = get_sub_field('expanded_grid');
?>
​
<section id="section<?= get_row_index(); ?>" class="section google-map" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
  <div class="grid-container <?= ($expanded_grid == true && $switch_columns == false ? 'grid-expanded' : 'full'); ?>">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-6 gmap<?= ($switch_columns == true ? ' reversed' : ''); ?>">
        <?php $locations = get_locations(null, 'home'); ?>
        <div id="tabs">
          <ol class="tabs-list">
            <?php foreach ( $locations as $location ) : ?>
              <li class="tab">
                <a href="#" data-location-id="<?php echo $location->ID; ?>"><?php echo $location->post_title; ?></a>
              </li>
            <?php endforeach; ?>
          </ol> <!-- .tabs-list -->
        </div> <!-- #tabs -->
      </div> <!-- .cell -->
​
      <div class="cell small-12 medium-12 large-6 copy">
        <div class="mapHolder">
          <div id="locations-map" style="width:100%; height:700px">
          </div>
        </div> <!-- .mapHolder -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->


