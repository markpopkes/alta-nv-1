/* global google, offsetCenter, AppData, mixitup */

var App = App || {},
  $ = $ || jQuery;

App.staticMap = function() {
  function initialize() {
    var lat = 36.0352249;
    var lng = -115.0417777;
    var mapOptions = {
      zoom: 14,
      scrollwheel: false,
      saturation: -100,
      center: new google.maps.LatLng(lat, lng),
      disableDefaultUI: true,
      styles: [{
        'featureType': 'administrative',
        'elementType': 'labels.text.fill',
        'stylers': [{
          'color': '#444444'
        }]
      },{
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [{
          'color': '#f2f2f2'
        }]
      },{
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [{
          'visibility': 'off'
        }]
      },{
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [{
            'saturation': -100
          },{
            'color': '#d2d0c9'
          },{
            'lightness': 45
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [{
          'visibility': 'simplified',
          'color': '#000000'
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [{
          'color': '#ffffff'
        }]
      },{
        'featureType': 'road.arterial',
        'elementType': 'labels.icon',
        'stylers': [{
          'visibility': 'off'
        }]
      },{
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [{
          'visibility': 'off'
        }]
      },{
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [{
              'color': '#dde6e8'
            },{
              'visibility': 'on'
        }]
      }]
    }
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var image = {
        url: AppData.template_dir + '/assets/images/pin.png',
        anchor: new google.maps.Point(35, 35)
    }
    var myLatLng = new google.maps.LatLng(36.0352249, -115.0417777);
    var stetsonMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        zIndex: 9999,
        url: 'https://goo.gl/maps/pjXipDzzmdmEU26fA',
    });

    google.maps.event.addListener(stetsonMarker, 'click', function() {
      window.open(stetsonMarker.url, '_blank');
    });

  } //initialize()

  if($('body').hasClass('home')) {
    google.maps.event.addDomListener(window, 'load', initialize);
  }

  if($('body').hasClass('contact')) {
    google.maps.event.addDomListener(window, 'load', initialize);
  }
} //App.staticMap

App.locationsMap = function() {
  var map_trigger = $('[data-location-id]'),
    map_options = {
      zoom : 13,
      mapTypeId : google.maps.MapTypeId.ROADMAP,
      //disableDefaultUI: true,
      draggable: true,
      styles: [{
        'featureType': 'administrative',
        'elementType': 'labels.text.fill',
        'stylers': [{
        'color': '#444444'
        }]
      },{
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [{
        'color': '#f2f2f2'
        }]
      },{
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [{
        'visibility': 'off'
        }]
      },{
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [{
            'saturation': -100
        },{
            'color': '#d2d0c9'
        },{
            'lightness': 45
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [{
        'visibility': 'simplified',
        'color': '#000000'
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [{
        'color': '#ffffff'
        }]
      },{
        'featureType': 'road.arterial',
        'elementType': 'labels.icon',
        'stylers': [{
        'visibility': 'off'
        }]
      },{
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [{
        'visibility': 'off'
        }]
      },{
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [{
            'color': '#dde6e8'
            },{
            'visibility': 'on'
        }]
      }]
    },
    map = new google.maps.Map(document.getElementById('locations-map'), map_options),
    infowindow = new google.maps.InfoWindow(),
    markers = [],
    infowindows = [];
  
  map_trigger.on('click', function(e) {
    e.preventDefault();
    var location_id = parseInt($(this).attr('data-location-id'));
    infowindow.close();
    infowindow.setContent(infowindows[location_id]);
    infowindow.open(map, markers[location_id]);
    map.panTo(markers[location_id].getPosition());
    markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
  });

  // Create bounds to help center map
  var bounds = new google.maps.LatLngBounds();
  var locations = AppData.locations;

  for (var i = 0; i < locations.length; i += 1) {
    (function() {
      var location = locations[i],
        latitude = location.custom_fields._wpseo_coordinates_lat,
        longitude = location.custom_fields._wpseo_coordinates_long,
        latLng = new google.maps.LatLng(latitude, longitude),
        marker = new google.maps.Marker({
          map: map,
          title: location.post_title,
          position: latLng,
          zIndex: 1,
        }),
        markerSlug = '',
        content = [
          '<div class="infowindow">',
          '<h4>' + location.post_title + '</h4>',
          '<p class="address">',
          location.custom_fields._wpseo_business_address + ',',
          location.custom_fields._wpseo_business_city + ', ',
          location.custom_fields._wpseo_business_state + ' ',
          location.custom_fields._wpseo_business_zipcode,
          '</p>',
          '</div>',
        ].join('');

        if ( location.category.slug === 'home' ) {
          markerSlug = location.category.slug;
        } else {
          markerSlug = i + 1;
        }
        var icon = { url: AppData.template_dir + '/assets/images/map-markers/marker-' + markerSlug + '.png' }
      
        bounds.extend(latLng);
        //console.log('locations', locations);
        //console.log('markers', markers);
        markers[location.ID] = marker;
        infowindows[location.ID] = content;
        marker.setIcon(icon);
        google.maps.event.addListener(marker, 'click', function() {
          infowindow.close();
          infowindow.setContent(content);
          infowindow.open(map, this);
          this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
        });
        google.maps.event.addDomListener(window, 'resize', function() {
          var mapCenter = map.getCenter();
          google.maps.event.trigger(map, 'resize');
          
          //map.setCenter(36.0352249, -115.039589);
        });
      
    })();
  }
  // Center map based on bounds
  map.setCenter(new google.maps.LatLng(36.0352249, -115.039589));
}

App.is_window_scrolled = function() {
  $(window).on('scroll', function(){
    var scroll = $(this).scrollTop();
    if (scroll >= 10) {
        $('body').addClass('scrolled');
    } else {
        $('body').removeClass('scrolled');
    }
  });
} //App.locationsMap()

App.headerPromoAdjust = function() {
  $(window).on('load', function() {
    // alert($('#promo-bar').css('display'));
    if ($('#promo-bar').css('display') === 'none' || $('#promo').css('display') === 'none') {
      $('header').css('top', '0');
    } else {
      $('header').css('top', '120px');
    }
  });

  $(window).on('scroll', function() {
    var scroll = $(this).scrollTop();
    if (scroll > 10) {
    //$('header').css('top', '0');
    } else if (scroll <= 10) {
      if ($('#promo-bar').css('display') === 'none' || $('#promo').css('display') === 'none') {
        $('header').css('top', '0');
      } else {
        $('header').css('top', '120px');
      }
    }
  });
}

App.fixed_cta = function() {
  $(window).scroll(function() {
    if($('body').hasClass('home')) {
      var distance = $('.contact-form').offset().top;
      var $window = $(window);
      if ($window.scrollTop() + 80 >= distance) {
      //if ($window.scrollTop() + 80 >= distance) {
        $('#cta-header .cta-button').removeClass('hidden');
        $('header').css('width', '100%');
      } else if ($window.scrollTop() <= distance) {
        $('#cta-header .cta-button').addClass('hidden');
      } //endif
    } //endif
  });
} //function

App.fixed_sidebar_onscroll = function() {
  $(window).scroll(function() {
    if($('body').hasClass('home')) {
      var distance = $('#section2').offset().top;
      var $window = $(window);
  
      if ($window.scrollTop() >= distance) {
        $('#header-sidebar').addClass('hidden');
        $('#sidebar').removeClass('hidden');
      } else if ($window.scrollTop() < distance) {
      //} else if ($window.scrollTop() <= distance) {
        // $('#cta-header .cta-button').addClass('hidden');
        $('#header-sidebar').removeClass('hidden');
        $('#sidebar').addClass('hidden');
      } //endif
    } //endif
  });
} //function

App.fixed_sidebar_onload = function() {
  $(document).ready(function() {
    if($('body').hasClass('home')) {
      var distance = $('#section3').offset().top;
      var $window = $(window);
      if ($window.scrollTop() >= distance) {
        console.log($window.scrollTop());
        // $('#cta-header .cta-button').removeClass('hidden');;
        $('#header-sidebar').addClass('hidden');
        $('#sidebar').removeClass('hidden');
      } else if ($window.scrollTop() <= distance) {
        // $('#cta-header .cta-button').addClass('hidden');
        $('#header-sidebar').removeClass('hidden');
        $('#sidebar').addClass('hidden');
      } //endif
    } //endif
  });
} //function

App.sidebar_link = function() {
  $('#header-sidebar, #sidebar').click(function() {
    window.location = $(this).find('#cta-sidebar a').attr('href'); 
    return false;
  });
} //function

App.promoClose = function() {
  $('.promo__close').on('click', function() {
    $('#promo, .promo__close').slideUp(250, function() {
      $('header').css('top', '0');
    });
    $('#logo').removeClass('hidden');
  });
  
}

App.expirePromo = function() {
  var timeInHours = .25
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration
  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);
  } else {
    $('#promo-bar').hide();
  }
} //function

App.navToggle = function() {
	$('.js-nav-toggle').on('click', function() {
    var promo_bar = $('#promo-bar').css('display');
    var header_nav = $('#access .header__nav');
    if (header_nav.css('visibility') === 'hidden' && promo_bar !== 'none') {
      //$('#logo').addClass('hidden');
    } else if (header_nav.css('visibility') === 'visible' && promo_bar !== 'none') {
      $('#logo').removeClass('hidden').addClass('visible');
    }
		if ( $('html').hasClass('is-open') ) {
      $('.nav-contain').fadeToggle(200);
      $('.button--nav').css('visibility', 'visible');
			$('html').toggleClass('is-open');
		} else {
      $('.nav-contain').fadeToggle(250);
      $('.button--nav').css('visibility', 'hidden');
			window.setTimeout( function() {
				$('html').toggleClass('is-open');
			}, 50 );
		}
	});
} //function

App.smoothScroll = function () {
  $('#scroll').on('click', function() {
      var cls = $(this).closest('.section').next().offset().top;
      $('html, body').animate({scrollTop: cls}, 'slow');
  });
} //function

App.floorPlans = function() {   
  $('#responsive-tabs').responsiveTabs({
    startCollapsed: 'accordion',
  });

  $('.r-tabs-panel .details a').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.details').next('.details-panel').find('.floor-plan-description').slideToggle('slow');

    if($(this).text() === 'View Floor Plan') {
      $(this).text('Hide Floor Plan');
    } else if ($(this).text() === 'Hide Floor Plan') {
      $(this).text('View Floor Plan');
    }
  });
} //function

App.imageSlider = function () {
  var $imgslider = $('.image-slider'),
    currentSlide,
    slideCount;
      
  $imgslider.each(function () {
    var $this = $(this);
    var slider_index = $this.data('index');
    var slideCounter = $('.slide-controls-' + slider_index + ' .slide-counter' );
    var updateSlideCounter = function (slick) {
        currentSlide = slick.slickCurrentSlide() + 1;
        slideCount = slick.slideCount;
        //console.log(slideCount); 
        slideCounter.html('<span class="slide-num">' + currentSlide + '</span> / <span class="slide-count">' + slideCount + '</span>');
    };
    
    $this.slick({
      dots: false,
      adaptiveHeight: true,
      appendArrows: $(this).next('.slide-controls'),
      prevArrow: '<a href="#" class="slick-prev prev-arrow"><div class="dashicons dashicons-arrow-left-alt"></div>',
      nextArrow: '<a href="#" class="slick-next next-arrow"><div class="dashicons dashicons-arrow-right-alt"></div>'
    });
    
    $this.on('afterChange', function (event, slick, currentSlide) {
        updateSlideCounter(slick, currentSlide); 
    });
  });
  
  $('.photos a').on('click', function (e) { 
    e.preventDefault();
    var getIndex = $(this).data('slide');
    var this_slider = $('.image-grid').data('slider');
    //console.log(this_slider);
    $('#image-' + this_slider).slick('slickGoTo', getIndex);
    $('html,body').stop().animate({
        'scrollTop': $('#image-' + this_slider).offset().top - 80
    }, 400, 'swing');
  });
  
  $('.tabs li a').on('click', function (e) {
      e.preventDefault();
      $('.tabs li, .tab-wrapper .tab-content').removeClass('current-tab');
      $(this).parent().addClass('current-tab');
      
      var currentTab = $(this).attr('href');
      //console.log(currentTab);
      $(currentTab).addClass('current-tab');
      $(currentTab + '-pager').addClass('current-tab');
      
      $imgslider.slick('setPosition', 0);
  });
}

App.fetchFloorPlans = function() {

  fetchFloorPlans();

  function fetchFloorPlans(e) {

    if ($('body').hasClass('residences')) {

      //var dataURL = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=floorPlan&apiToken=d1eaffc8-ec15-4be1-9e5e-d2b393f3b9eb&PropertyId=1127773';
      var dataURL = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=floorplan&apitoken=d1eaffc8-ec15-4be1-9e5e-d2b393f3b9eb&propertyCode=p1102722';
      //var dataURL = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=apartmentavailability&apitoken=d1eaffc8-ec15-4be1-9e5e-d2b393f3b9eb&propertyCode=p1102722';
      var filter = $('#floor-tabs');
      var showData = $('.floor-plan-wrapper');

      $.getJSON( dataURL, function(data) {
        showData.empty();
        var content = '';
        var filterItems = '';
        var unitDivs = '';
        var units = new Array();
        var units2 = new Array();
        var setDelay = 300;
        var timeDelay = 0;
        var f_data = data.sort(function (x,y) {
            return ((x.MaximumRent == y.MaximumRent) ? 0 : ((x.MaximumRent > y.MaximumRent) ? 1 : -1 ));
        });
              
        for( var i=0; i<f_data.length; i++ ) {
          timeDelay = timeDelay + setDelay;
          item = data[i];

          //console.log(item);
          
          var floorName = item.FloorplanName;

          console.log(floorName);
          
          nameSlug = floorName.replace(/\s/g, '');
          var unitName = '';
          var slugName = '';
          var price = item.MinimumRent == '' ? 'Price Coming Soon' : '$' + item.MinimumRent;

          if( item.Beds == 0 ) {
            unitName = 'Studio';
                      slugName = 'studio';
          }
          if(item.Beds == 1 ) {
            unitName = 'One Bedroom';
                      slugName = 'one-bedroom';
          }
          if( item.Beds == 2 ) {
            unitName = 'Two Bedroom';
                      slugName = 'two-bedrooms';
          }
          if( item.Beds == 3 ) {
            unitName = 'Three Bedrooms';
                      slugName = 'three-bedrooms';
          }

          if( $.inArray(data[i].Beds, units) == -1) units.push(data[i].Beds);

          content += '<div class="grid-x floor-unit mix '+ slugName +'">';
          content += '<div class="cell large-3">Floor Plan ' + floorName +'</div>';
          content += '<div class="cell large-3"><span>'+ parseFloat(item.Baths) +' Bath</span></div>';
          content += '<div class="cell large-3"><span>'+  price +'</span></div>';
          content += '<div class="cell large-3"><a href="#diagram-' + floorName +'" class="button primary floor-plan-button" data="view-plan">View Floor Plan</a></div>';

          // diagram WITH apply now button
          content += '<div id="diagram-' + floorName + '" class="cell large-12 diagram section collapse"><img class="floor-plan-img" src="'+ AppData.template_dir + '/assets/images/floorplans/' + floorName +'.png" alt=""/><a href="https://www.on-site.com/apply/property/292499" class="button dark" target="_blank">Apply Now</a></div>';
          content += '</div>';
        }

        showData.append(content);
              
        for( var j=0; j<units.length; j++ ) {

          var unitName = ''
            slugName = '';

          if( units[j] == 0 ) {
            unitName = 'Studio';
                      slugName = 'studio';
          }
          if(units[j] == 1 ) {
            unitName = 'One Bedroom';
                      slugName = 'one-bedroom';
          }
          if( units[j] == 2 ) {
            unitName = 'Two Bedroom';
                      slugName = 'two-bedrooms';
          }
          if( units[j] == 3 ) {
            unitName = 'Three Bedroom';
                      slugName = 'three-bedrooms';
          }

          filterItems += '<li class="units-'+ units[j]+'"><a href="javascript:;" title="'+ unitName +'" data-filter=".'+ slugName +'">'+ unitName +'</a></li>';
        }
        
        filter.html(filterItems);

      }).done(function() {
      
        var mixer = mixitup('.floor-plan-wrapper', {
          animation: {
            effects: 'fade translateZ(-100px)'
          },
          load: {
            filter : '.one-bedroom'
          }
        });

        $('.floor-plan-button').on('click', function(e) {
          e.preventDefault();
          $(this).parent().next('.collapse').slideToggle('slow');
      
          if($(this).text() === 'View Floor Plan') {
            $(this).text('Hide Floor Plan');
          } else if ($(this).text() === 'Hide Floor Plan') {
            $(this).text('View Floor Plan');
          }
        });

      }).fail(function() {
        var error_message = '<p>We had trouble loading the floor plans. Please try again later</p>';
        showData.html( error_message );
      });
    } //endif
  } //function
}

if ( $('body').hasClass('neighborhood') ) {
  App.locationsMap();
}

if ( $('body').hasClass('contact') || $('body').hasClass('home') ) {
  App.staticMap();
}

//App.headerPromoAdjust();
App.fetchFloorPlans();
App.floorPlans();
App.is_window_scrolled();
App.fixed_cta();
App.fixed_sidebar_onscroll();
App.fixed_sidebar_onload();
App.sidebar_link();
App.promoClose();
App.expirePromo();
App.navToggle();
App.smoothScroll();
App.imageSlider();
