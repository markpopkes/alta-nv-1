<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>

<section class="section error error-404 not-found">
	<div class="container">
		<header class="error__header">
			<h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.'); ?></h1>
		</header>

		<div class="error__content">
			<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?'); ?></p>

			<?php get_search_form(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
