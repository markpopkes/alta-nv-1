<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<main role="main">

	<section class="section">
		<div class="container">

			<?php if ( have_posts() ) : ?>

				<header>
					<?php
						the_archive_title( '<h1>', '</h1>' );
						the_archive_description( '<div class="archive__description">', '</div>' );
					?>
				</header>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'partials/content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>

				<?php get_template_part( 'partials/content', 'none' ); ?>

			<?php endif; ?>

		</div>
	</section>

</main>

<?php get_footer(); ?>
