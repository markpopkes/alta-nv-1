<?php
  
  $hero_image = get_sub_field('hero_image');
  $headline = get_sub_field('headline'); ?>

  <?php if(get_row_layout() == 'hero_image_with_headline'): ?>

  <?php elseif (get_row_layout() == 'content_boxes_3_column'): $tagline = get_sub_field('tagline'); ?>

  <section id="section<?= get_row_index(); ?>" class="section" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
    <div class="grid-container fluid quote">
      <div class="grid-x">
        <div class="cell small-12 medium-12 large-12">
          <p><?= $tagline; ?></p>
        </div> <!-- .cell -->
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->
  
    <div class="grid-container boxes">
      <div class="grid-x">
        <?php 
          $count = 1; 
          $sidebar_image = get_field('featured_image');
          $sidebar_large_headline = get_field('sidebar_large_headline');
          $sidebar_small_headline = get_field('sidebar_small_headline');
          $sidebar_button_text = get_field('sidebar_button_text');

          if (have_rows('content_boxes')): while (have_rows('content_boxes')): the_row(); 
            $bg_image = get_sub_field('background_image'); 
            $title = get_sub_field('headline'); 
            $copy = get_sub_field('content'); 
            $bg_image = get_sub_field('background_image'); ?>
          <div class="cell small-12 medium-4 large-3 box box<?=$count;?>" style="<?= (!empty($bg_image) ? 'background-image:url('.$bg_image["url"].')' : ''); ?>">
            <div class="textarea">
              <h3><?php echo $title; ?></h3>
              <?= $copy; ?>
            </div> <!-- .textarea -->
          </div> <!-- .cell -->
        <?php $count++; endwhile; endif; ?>
        <div class="cell small-12 medium-3 large-3 box4"></div>
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->

    <div id="sidebar" class="sidebar">
      <div class="sidebar-fixed">
        <img src="<?= (!empty($sidebar_image) ? $sidebar_image['url'] : ''); ?>" alt="Alta NV" />
        <div class="textbox-sidebar">
          <h3><?= $sidebar_large_headline; ?></h3>
          <p><?= $sidebar_small_headline; ?></p>
        </div> <!-- .textbox -->

        <div id="cta-sidebar">
          <a href="/contact/" class="cta-button" title="Reserve Now"><?= $sidebar_button_text; ?></a>
        </div> <!-- .cta-sidebar -->
        <!--<span class="heroTab">Text Us</span>-->
      </div> <!-- .sidebar-fixed -->
    </div> <!-- #sidebar -->
  </section>

  <?php elseif (get_row_layout() == 'contact_section'): ?>

  <section id="section<?= get_row_index(); ?>" class="section grid-container full contact-form">
    <div class="grid-x">
      <div class="cell small-12 large-6">
        <div id="contact-form">
          <div class="contact-form__title">
            <h2>Get In Touch</h2>
            <div id="social">
              <a class="facebook" href="https://facebook.com" target="_blank" style="visibility:hidden"></a>
              <a class="instagram" href="https://instagram.com" target="_blank" style="visibility:hidden"></a>
            </div> <!-- #social -->
          </div> <!-- .contact-form__title -->
          <p>What will you discover while living a lifestyle worthy of NV? Get in touch with us to learn more about living at Alta NV.</p>
          
          <div id="cta">
            <a href="/contact/#contact-form" class="cta-button" title="Schedule a Tour">Get In Touch</a>
          </div> <!-- .cta-sidebar -->
        </div> <!-- #contact-form -->
      </div>

      <div class="cell small-12 large-6">
        <div class="mapHolder">
          <div id="map" style="width:100%"></div>
        </div> <!-- .mapHolder -->
      </div>
    </div> <!-- .grid-x -->
  </section>

<?php endif; ?>

